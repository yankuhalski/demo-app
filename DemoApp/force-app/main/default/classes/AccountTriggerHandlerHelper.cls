public class AccountTriggerHandlerHelper {

    private static final String SUBJECT = 'Negotiation';
    private static final String STATUS = 'Not Started';

      @Future
      public static void createTaskToNegotiationWith(Set<Id> accountsId){

         List<Task> tasks = new List<Task>();
         Map<Id,Account> accounts = new Map<Id,Account>([SELECT Id , OwnerId FROM Account WHERE Id IN : accountsId]);
       
         for(Id accountId : accounts.keySet()){
            tasks.add(
               new Task (  WhatId = accountId,
                           OwnerId = accounts.get(accountId).OwnerId,
                           Subject = SUBJECT,
                           Status = STATUS,
                           Is_Synced__c = false ));
         }
         insert tasks;
      }

      public static void changeContactsIfBillingAddressChanged(List<Account> updatedAccounts, Map<Id, Account> oldAccountsMap){
         List<Id> accountsIdWithChangedBillingAddress = new List<Id>();
         
         for (Account account : updatedAccounts){

            Account oldAccount = oldAccountsMap.get(account.Id);

               if(oldAccount.BillingCountry != account.BillingCountry ||
                    oldAccount.BillingCity != account.BillingCity ||
                    oldAccount.BillingStreet != account.BillingStreet ||
                    oldAccount.BillingState != account.BillingState ||
                    oldAccount.BillingPostalCode != account.BillingPostalCode){

                  accountsIdWithChangedBillingAddress.add(account.Id);
               }
         }

         if(!accountsIdWithChangedBillingAddress.isEmpty()){
            changeContacts(accountsIdWithChangedBillingAddress);
            System.enqueueJob(new UpdateContactsJob(accountsIdWithChangedBillingAddress));
         }
      }

      @Future
      public static void changeContacts(List <Id> accountsId){
         List<Contact> contactsToUpdate = new List<Contact> ([SELECT Id, Is_Synced__c, Processed_By_Future__c FROM Contact WHERE Account.Id In : accountsId]);
            for(Contact contact : contactsToUpdate){
               contact.Is_Synced__c = false;
               contact.Processed_By_Future__c = true;
            }
        update contactsToUpdate;
        
      }

}