public  class TaskTriggerHandlerHelper {
    private static final String SUBJECT_NEGOTIATION = 'Negotiation';
    private static final String STATUS_COMPLETED = 'Completed';


    public static void changeAccountSeeingWithPlanedNegotiation(List<Task> newTasks){
        List<Id> accountsIds = new List<id>(); 
        for(Task task : newTasks){
            if(task.Subject.equals(SUBJECT_NEGOTIATION) && !task.Status.equals(STATUS_COMPLETED) ){    
             accountsIds.add(task.WhatId);
            }
        }
        List<Account> accounts = [SELECT Id, Plans_Discussed__c FROM Account where Id in : accountsIds];
        for (Account account : accounts) {
            if(account.Plans_Discussed__c){
            account.Plans_Discussed__c = false;
        }}
        update accounts;
    }

    public static void changeAccountAfterNegotiation(List<Task> changedTasks, Map<Id, Task> oldObjectsMap){
        List<Id> accountsIds = new List<id>(); 

        for(Task task : changedTasks){

            if(task.Status.equals(STATUS_COMPLETED) && 
             task.Subject.equals(SUBJECT_NEGOTIATION) && 
             !oldObjectsMap.get(task.Id).Status.equals(STATUS_COMPLETED)){ 
             accountsIds.add(task.WhatId);
            }
        }

        List<Account> accounts = new List<Account>([SELECT Id,Plans_Discussed__c FROM Account WHERE Id in : accountsIds]);
        for (Account account : accounts) {
             if(!account.Plans_Discussed__c){
            account.Plans_Discussed__c = true;
        }}
        update accounts;
    }
  
}