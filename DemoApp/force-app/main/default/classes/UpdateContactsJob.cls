public class UpdateContactsJob implements Queueable {
    List<Id> accountsId ; 

    public UpdateContactsJob(List <Id> accountsId) {
        this.accountsId = accountsId ;
    }

    public void execute(QueueableContext context) {
        List<Contact>  contactsToUpdate = new List<Contact>([SELECT ID FROM Contact WHERE Account.Id In : accountsId]);
        for(Contact contact : contactsToUpdate){
            contact.Is_Synced__c = false;
            contact.Processed_By_Queue__c = true;
        }
        update contactsToUpdate;
    }
}