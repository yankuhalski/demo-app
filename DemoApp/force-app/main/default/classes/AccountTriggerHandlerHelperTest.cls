@isTest
public class AccountTriggerHandlerHelperTest {
    private final static Integer ACCOUNTS_AMOUNT = 10;
    
     @TestSetup
    static void makeData(){
 
        List<Account> accounts = new List<Account>();

        for ( Integer i = 0 ; i < ACCOUNTS_AMOUNT ; i ++ ) {
             Account newAccount = new Account ( Name = 'Dude' + i);
             accounts.add(newAccount);
        } 
        insert accounts ;
             
        List<Contact> contacts = new List<Contact>();

        for (Account account : accounts){
             Contact newContact = new Contact( LastName = account.Name + 'Contact',	AccountId = account.Id);
             contacts.add(newContact);
        }
        insert contacts ;
    }


    @isTest
    static void shouldCreateTaskConnectedWithNewAccounts(){

        Map<Id,Account> accountsMap = new Map<Id,Account>([SELECT Id FROM Account]);
        Set<Id> ids = accountsMap.keySet();

        Test.startTest();
        delete new List<Task>([SELECT Id FROM Task]);

        AccountTriggerHandlerHelper.createTaskToNegotiationWith(ids);
        Test.stopTest();
        

        Integer amountOfNewTasks = Database.countQuery('SELECT count() FROM Task WHERE WhatId in :ids');
        
        System.assertEquals(10, amountOfNewTasks);
    }

    @isTest
    static void  shouldChangeContacts(){
        Map<Id,Account> accountsMap = new Map<Id,Account>([SELECT Id FROM Account]);
        List<Id> ids = new List<Id>(accountsMap.keySet());

        Test.startTest();

        AccountTriggerHandlerHelper.changeContacts(ids);
        Test.stopTest();

        Integer unchangedContacts = Database.countQuery(' SELECT count() FROM Contact   WHERE Account.Id In : ids  AND  Is_Synced__c = true AND Processed_By_Future__c = false');
       
        System.assertEquals(0, unchangedContacts);
    }

}