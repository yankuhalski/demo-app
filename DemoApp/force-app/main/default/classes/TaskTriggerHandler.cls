public class TaskTriggerHandler extends TriggerHandler {

    public override  void onBeforeInsert(List<SObject> newObjects)  {
        TaskTriggerHandlerHelper.changeAccountSeeingWithPlanedNegotiation((List<Task>) newObjects);
    }

    public override void onAfterUpdate(List<sObject> updatedObjects, Map<Id, sObject> oldObjectsMap) {
        TaskTriggerHandlerHelper.changeAccountAfterNegotiation((List<Task>)updatedObjects, (Map<Id, Task>) oldObjectsMap);
    } 
}