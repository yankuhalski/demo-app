@IsTest
public class TaskUpdateBatchJobTest {
    
    private final static Integer ACCOUNTS_AMOUNT = 10;

     @TestSetup
    static void makeData(){
        List<Account> accounts = new List<Account>();

        for ( Integer i = 0 ; i < ACCOUNTS_AMOUNT ; i ++ ) {
             Account newAccount = new Account ( Name = 'Dude ' + i, BillingCity ='Some City');
             accounts.add(newAccount);
        } 
        insert accounts ;
    }

     @isTest 
    public static void shouldSynchTask(){
        Test.startTest();
        Id id = Database.executeBatch(new TaskUpdateBatchJob());
        Test.stopTest();
        List<Task> contacts = new List<Task>([SELECT Id, WhatId, TYPEOF  What WHEN Account THEN Id END FROM Task WHERE Is_Synced__c = false]);
        System.assertEquals(0, contacts.size());
    }
}