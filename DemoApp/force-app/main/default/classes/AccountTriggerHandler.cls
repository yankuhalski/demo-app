public class AccountTriggerHandler extends TriggerHandler {
    private static  Boolean allReadyWorlk = true;

    public override void onAfterInsert(List<sObject> newObjects, Map<Id, sObject> newObjectsMap) {
        AccountTriggerHandlerHelper.createTaskToNegotiationWith(newObjectsMap.keySet());
    }

    public override void onAfterUpdate(List<sObject> updatedObjects, Map<Id, sObject> oldObjectsMap) {
        AccountTriggerHandlerHelper.changeContactsIfBillingAddressChanged( (List<Account>) updatedObjects, (Map<Id, Account>) oldObjectsMap);    
    }

}