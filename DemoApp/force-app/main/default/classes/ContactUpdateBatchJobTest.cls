@IsTest
public with sharing class ContactUpdateBatchJobTest {

    private final static Integer ACCOUNTS_AMOUNT = 10;

     @TestSetup
    static void makeData(){
        List<Account> accounts = new List<Account>();

        for ( Integer i = 0 ; i < ACCOUNTS_AMOUNT ; i ++ ) {
             Account newAccount = new Account ( Name = 'Dude ' + i, BillingCity ='Some City');
             accounts.add(newAccount);
        } 
        insert accounts ;
             
        List<Contact> contacts = new List<Contact>();

        for (Account account : accounts){
             Contact newContact = new Contact( LastName = account.Name + 'Contact',	AccountId = account.Id, Is_Synced__c = false);
             contacts.add(newContact);
        }
        insert contacts ;
    }


    @isTest 
    public static void shouldSynchContacts (){

        Test.startTest();
        Id id = Database.executeBatch(new ContactUpdateBatchJob());
        Test.stopTest();

        List<Contact> updatedContacts = new List<Contact> ([SELECT Id FROM Contact WHERE MailingCity = 'Some City' ]); 
        System.assertEquals(10, updatedContacts.size());
    }
}