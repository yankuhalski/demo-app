public class TaskUpdateBatchJob  implements Database.Batchable<SObject> {

    public Database.QueryLocator start(Database.BatchableContext context){
        return Database.getQueryLocator('SELECT Id, WhatId, TYPEOF  What WHEN Account THEN Id END FROM Task WHERE Is_Synced__c = false');
    }

    public void execute (Database.BatchableContext context , List<Task> tasks){
        List<Id> whatIds = new List<Id>();
        for(Task task : tasks){
            whatIds.add(task.WhatId);
        }

        Map <Id,Account> accounts = new Map<Id,Account>([SELECT Id, Owner.Name FROM Account WHERE Id IN : whatIds]);

        for (Task task : tasks){
            Account whatAccount =  accounts.get(task.WhatId);
            task.Account_Owner__c = whatAccount.Owner.Name;
            task.Is_Synced__c = true;
            whatAccount.Updated_By_Task__c = true;
        }

        update tasks ;
        update accounts.values();
    }

    public void finish(Database.BatchableContext context){}
}