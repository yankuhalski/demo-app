public class BatchShaduler  implements Schedulable{

    public static final String CRON_EXP_00 = '0 0 * ? * *';
    public static final String CRON_EXP_30 = '0 30 * ? * *';

    public void execute(SchedulableContext sc){
        TaskUpdateBatchJob taskBatch = new TaskUpdateBatchJob();
        ContactUpdateBatchJob contactBatch = new ContactUpdateBatchJob();
 
        Database.executeBatch(taskBatch,200);
        Database.executeBatch(contactBatch,200);
    }

}