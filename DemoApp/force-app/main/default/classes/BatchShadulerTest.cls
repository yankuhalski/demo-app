@isTest
public  class BatchShadulerTest {
   private final static Integer ACCOUNTS_AMOUNT = 10;

     @TestSetup
    static void makeData(){
        List<Account> accounts = new List<Account>();

        for ( Integer i = 0 ; i < ACCOUNTS_AMOUNT ; i ++ ) {
             Account newAccount = new Account ( Name = 'Dude ' + i, BillingCity ='Some City');
             accounts.add(newAccount);
        } 
        insert accounts ;
             
        List<Contact> contacts = new List<Contact>();

        for (Account account : accounts){
             Contact newContact = new Contact( LastName = account.Name + 'Contact',	AccountId = account.Id, Is_Synced__c = false);
             contacts.add(newContact);
        }
        insert contacts ;
    }
    
    @isTest
    static void shadulerWillStart (){
        String shadId = System.schedule('id1122334455667788', BatchShaduler.CRON_EXP_30, new BatchShaduler());

        List<CronTrigger> cronTriggers = new List<CronTrigger>([
                SELECT CronExpression
                FROM CronTrigger
                WHERE Id = :shadId 
        ]);
        System.assertEquals(1, cronTriggers.size());

    }
}