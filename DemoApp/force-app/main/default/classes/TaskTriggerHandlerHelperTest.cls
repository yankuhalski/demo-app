@isTest
public class TaskTriggerHandlerHelperTest {
     
    private static final String SUBJECT_NEGOTIATION = 'Negotiation';
    private static final String STATUS_NOT_STARTED ='Not Started';
    private final static Integer ACCOUNTS_AMOUNT = 10;

    @TestSetup
    static void makeData(){
 
        List<Account> accounts = new List<Account>();

        for ( Integer i = 0 ; i < ACCOUNTS_AMOUNT ; i ++ ) {
            accounts.add( new Account ( Name = 'Dude' + i));
        } 
             
        insert accounts ;
    }

    @isTest
    static void shouldSetAccountPlansDiscussedFalse (){
       
        List<Task> tasks = new List<Task>([SELECT Id , WhatId FROM Task WHERE Status =: STATUS_NOT_STARTED AND Subject =: SUBJECT_NEGOTIATION ]);
        for(Task task : tasks){
            task.Status = 'Completed';
        }
        update tasks;

        List<Task> newTasks = new List<Task>();
        List<Account> accounts = new List<Account> ([SELECT Id , OwnerId FROM Account]);
        for(Account account : accounts){
           newTasks.add(
               new Task (   WhatId = account.Id, 
                            OwnerId = account.OwnerId, 
                            Subject = SUBJECT_NEGOTIATION,
                            Status = STATUS_NOT_STARTED));
        }
        insert newTasks;
        Integer amountOfErrors = Database.countQuery('SELECT count()  FROM Account WHERE Plans_Discussed__c = true');
        System.assertEquals( 0 , amountOfErrors);

    }

    @isTest
    static void shouldSetAccountPlansDiscussedTrue (){

        List<Task> tasks = new List<Task>([SELECT Id , WhatId FROM Task WHERE Status ='Not Started' AND Subject = 'Negotiation']);
        List<Id> clientsId = new List<Id>();

        for(Task task : tasks){
            clientsId.add(task.WhatId);
            task.Status = 'Completed';
        }

        update tasks;

        Integer amountOfErrors = Database.countQuery('SELECT count() FROM Account WHERE id in : clientsId AND Plans_Discussed__c = false');

        System.assertEquals( 0 , amountOfErrors);
    }

}