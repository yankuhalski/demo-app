public class ContactUpdateBatchJob implements Database.Batchable<SObject> {
 
     public Database.QueryLocator start(Database.BatchableContext context){
        return Database.getQueryLocator('SELECT Id, AccountId FROM Contact WHERE Is_Synced__c = false');
    }

    public void execute (Database.BatchableContext context , List<Contact> contacts){
        List<Id> accountIds = new List<Id>();
        for(Contact contact : contacts){
            accountIds.add(contact.AccountId);
        }

        Map <Id,Account> accounts = new Map<Id,Account>([SELECT Id, BillingCity, BillingCountry, BillingPostalCode, BillingState, BillingStreet, Owner.Name FROM Account WHERE Id IN : accountIds]);

        for (Contact contact : contacts){
            Account account =  accounts.get(contact.AccountId);

            contact.MailingCity = account.BillingCity;
            contact.MailingCountry = account.BillingCountry;
            contact.MailingPostalCode = account.BillingPostalCode;
            contact.MailingState = account.BillingState;
            contact.MailingStreet = account.BillingStreet;

            contact.Is_Synced__c = true;
            account.Updated_By_Contact__c = true;
        }

        update contacts ;
        update accounts.values();
    }

    public void finish(Database.BatchableContext context){}
}